package domain;

import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;





@RestController
public class BankControl {

    public boolean typeEval(double _field) {
        if (_field == 0) {
            return false;
        }
        else {
            return true;            
            }
        }

    public boolean typeEval(String _field) {
        if (_field == null) {
            return false;
        }
        else {
            return true;            
            }
        } 
        

    public boolean validateTransactionInfo(Statement statement) {
        return !(typeEval(statement.getTransactionReference())&&typeEval(statement.getAcountNumber()));
        }


    public boolean duplication(double _accountNumber, Statement _matchedStatament) {
        return _accountNumber == _matchedStatament.getTransactionReference();
    }

    protected boolean checkEndBalance(Statement _statement) {
        return _statement.getEndBalance()>=0;
    }


    public boolean mutationCheck(Statement _statement) {
        return ((_statement.getMutation()+_statement.getStartBalance())==_statement.getEndBalance());
    }


    @PostMapping("/api/open")
    protected ResponseEntity<String> validateJsonOpen(@RequestBody @Valid Statement statement, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(400).body("json was incorrect");
        }
        return openTransaction(statement);
    }

    protected ResponseEntity<String> openTransaction(@RequestBody Statement statement)
        {
            
        List<Double> requestedStatements = new ExistingStatements().existingStatements;    

        if (!(typeEval(statement.getTransactionReference()))) {
            return ResponseEntity.status(400).body("Transaction does not have an accountNumber or reference");}

        if(!checkEndBalance(statement)) {
            return ResponseEntity.status(409).body("Endbalance was incorrect");}

        for (Double iterator : requestedStatements) {             
            if(duplication(iterator,statement)) {
                return ResponseEntity.status(409).body("Duplicate reference");}
            }

        if(!mutationCheck(statement)) {
                return ResponseEntity.status(400).body("Validation failed, mutation was incorrect");}
        
        return ResponseEntity.ok("Transaction succesfull: ");
    }



    private final String API_KEY = "balance-role";

    @PreAuthorize("('editor')")
    @PostMapping("/api/security")
    protected ResponseEntity<String> validateJsonSecure(@RequestBody @Valid Statement statement, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(400).body("json was incorrect");
        }
        return openTransaction(statement);
    }
    protected ResponseEntity<String> secureTransaction(@RequestHeader(name = "Api-Key") String apiKey,@RequestBody Statement statement)
        {

        if (!API_KEY.equals(apiKey)) {
            return ResponseEntity.status(401).body("user unauthorized for editor");
        } 

        List<Double> requestedStatements = new ExistingStatements().existingStatements;    

        if (!validateTransactionInfo(statement)) {
            return ResponseEntity.status(400).body("Transaction does not have an accountNumber or reference");}

        if(!checkEndBalance(statement)) {
            return ResponseEntity.status(409).body("Endbalance was incorrect");}

        for (Double iterator : requestedStatements) {             
            if(duplication(iterator,statement)) {
                return ResponseEntity.status(409).body("Duplicate reference");}
            }

        if(!mutationCheck(statement)) {
                return ResponseEntity.status(400).body("Validation failed, mutation was incorrect");}
        
        return ResponseEntity.ok("Transaction succesfull: ");
    }
}
    




