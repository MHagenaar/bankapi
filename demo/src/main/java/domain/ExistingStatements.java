package domain;

import java.util.ArrayList;
import java.util.List;



public class ExistingStatements {
    public List<Double> existingStatements = new ArrayList<>();
    
    public ExistingStatements() {
        this.existingStatements.add(1.0);
        this.existingStatements.add(2.0);
        this.existingStatements.add(3.0);
        this.existingStatements.add(4.0);
      
    }
    
}