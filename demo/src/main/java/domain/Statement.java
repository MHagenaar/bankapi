package domain;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class Statement {

    @NotNull
    @NotBlank
    public double TransactionReference;

    @NotNull
    @NotBlank
    public double AcountNumber;
    
    @NotBlank
    public double StartBalance;
    
    @NotBlank
    public double Mutation;
    
    public String FreeText;

    @NotBlank
    public double EndBalance;



    Statement(double TransactionReference,double AcountNumber,double StartBalance,double Mutation,String FreeText,double EndBalance) {
        this.TransactionReference = TransactionReference;
        this.AcountNumber = AcountNumber; 
        this.StartBalance = StartBalance;
        this.Mutation = Mutation;
        this.FreeText = FreeText;
        this.EndBalance = EndBalance;
    }



    public double getTransactionReference() {
        return TransactionReference;
    };

    public double getAcountNumber() {
        return AcountNumber;
    };

    public double getStartBalance() {
        return StartBalance;
    };

    public double getMutation() {
        return Mutation;
    };

    public String getFreeText() {
        return FreeText;
    };

    public double getEndBalance() {
        return EndBalance;
    };


}
