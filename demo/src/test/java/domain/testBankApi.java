package domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;



@SpringBootTest
@SpringJUnitConfig
public class testBankApi{

    public testBankApi() {
    }
   
    @Test
    public void testInitializationForJson() {
        Statement statement = new Statement(1,1,1,1,"1",1);
        assertEquals(statement.getAcountNumber(),1,0);
    }

    @Test
    public void testInitializationWithDataForJson() {
        Statement statement = new Statement(1,1,1,1,"1",1);
        assertEquals(statement.getAcountNumber(),1,0);
    }

    @Test
    public void testTypeEvaln0Result() {
        BankControl bankControl = new BankControl();
        assertEquals(bankControl.typeEval(0),false);
    }

    @Test
    public void testTypeEvalnullResult() {
        BankControl bankControl = new BankControl();
        assertEquals(bankControl.typeEval(null),false);
    }

    @Test
    public void testTypeEvalnumberResult() {
        BankControl bankControl = new BankControl();
        assertEquals(bankControl.typeEval(3),true);
    }

    @Test
    public void getStatementListWith0accountnumber() {
        BankControl bankControl = new BankControl();
        Statement statement = new Statement(0,1,1,0,"0",1);
        assertEquals(bankControl.typeEval(statement.getAcountNumber()),true);
    }

    @Test
    public void getStatementListWith0reference() {
        BankControl bankControl = new BankControl();
        Statement statement = new Statement(1,0,1,1,"1",1);
        assertEquals(bankControl.typeEval(statement.getTransactionReference()),true);
    }


    @Test
    public void mutationCheckForValidTransaction() {
        BankControl bankControl = new BankControl();
        Statement statement = new Statement(0,1,1,1,"1",2);
        assertEquals(bankControl.mutationCheck(statement),true);
    }

    @Test
    public void mutationCheckForFalseTransaction() {
        BankControl bankControl = new BankControl();
        Statement statement = new Statement(0,1,1,-1,"1",2);
        assertEquals(bankControl.mutationCheck(statement),false);
    }

    @Test
    public void checkTransactionSameReference() {
        BankControl bankControl = new BankControl();
        Statement matchingstatement = new Statement(0,1,1,-1,"1",2);
        Statement statement = new Statement(0,1,1,-1,"1",2);
        assertEquals(bankControl.duplication(statement, matchingstatement),true);
    }   
    
    @Test
    public void checkTransactionDifferentReference() {
        BankControl bankControl = new BankControl();
        Statement matchingstatement = new Statement(0,1,1,-1,"1",2);
        Statement statement = new Statement(1,1,1,-1,"1",2);
        assertEquals(bankControl.duplication(statement, matchingstatement),false);
    }   
    

}

